# Hanakopatch

## これはなに？

[TH12.5 ダブルスポイラー](https://en.touhouwiki.net/wiki/Double_Spoiler)のScene 12-6 正体不明「厠の花子さん」で発生するバグの修正パッチです。

以下のバグを修正します。

 * Scene 12-6のリプレイで発生するリプレイずれ(desync)
 * Scene 12-6で青弾が画面上部以外で発生するバグ

Scene 12-6のバグとしては[突然被弾するバグ](https://www26.atwiki.jp/touhoufuguaimatome/pages/162.html)が有名ですが、このバグが直るかどうかは未検証です。

## 使い方

[SpoilerAL](http://wcs.main.jp/index/software/spal/)で`th125_hanakopatch.ssg`を指定してください。

## リプレイの互換性

| リプレイの保存 | リプレイの再生 | ok/NG |
|----------------|----------------|-------|
| 製品版ver 1.00a | 製品版ver 1.00a | NG |
| 製品版ver 1.00a | 製品版ver 1.00a + hanakopatch | NG |
| 製品版ver 1.00a + hanakopatch | 製品版ver 1.00a | NG |
| 製品版ver 1.00a + hanakopatch | 製品版ver 1.00a + hanakopatch | ok |
                   
## バグの仕組み

### TL;DR

初期化してないフィールドを青弾の初期位置に使ったのが悪い。

### Detail

青弾の位置は、あるオブジェクトAのフィールドBの値を使って初期化される。
このオブジェクトA自体は、アドレス0x410240-0x410482のコードで初期化されるが、フィールドBは初期化されていない。
そのため、オブジェクトAの領域にもともとあったメモリ内容がフィールドBの値として使われる。

オブジェクトAはヒープ領域から確保されるが、この領域は必ずしも0クリアされておらず、古いデータで汚れていることもある。
これは古いデータによってフィールドBが汚れ、さらに青弾の位置も汚れることを意味する。

フィールドBの汚れ方はゲームプレイ毎、リプレイ再生毎に変わりうる。
そのため、以下の現象が起きる。

 * ゲームプレイ、リプレイ再生のたびに青弾の発生位置が変わる
 * 青弾の発生位置が変わったことで乱数も変化し、レーザーなど他の要素も変化する

## 修正の仕組み

フィールドBが初期化されていないのが原因なので、hanakopatchではフィールドBを適切に初期化することで解決している。

具体的には以下の変更を行っている。

```
; Before
00410F4E   5B               POP EBX
00410F4F   C2 0400          RETN 4
00410F52   CC               INT3
00410F53   CC               INT3
00410F54   CC               INT3
00410F55   CC               INT3
00410F56   CC               INT3
00410F57   CC               INT3
00410F58   CC               INT3

; After
00410F4E   895F 10          MOV DWORD PTR DS:[EDI+10],EBX ; initialize fields to 0
00410F51   895F 14          MOV DWORD PTR DS:[EDI+14],EBX
00410F54   5B               POP EBX
00410F55   C2 0400          RETN 4
00410F58   CC               INT3
```

もともとフィールドBは古いデータで汚れるものの、多くの場合0で初期化される。
そのため、hanakopatchでは必ず0で初期化することで青弾の発生位置変化とリプレイずれを解消した。
